define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ordersystem/shop/index',
                    add_url: 'ordersystem/shop/add',
                    edit_url: 'ordersystem/shop/edit',
                    del_url: 'ordersystem/shop/del',
                    multi_url: 'ordersystem/shop/multi',
                    table: 'shop',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'id',width:'5%', title: __('Id')},
                        {field: 'merchant_id',width:'5%', title: __('Merchant_id')},
                        {field: 'shop_status', title: __('Shop_status'), searchList: {"1":__('Shop_status 1'),"2":__('Shop_status 2')}, formatter: Table.api.formatter.status},
                        {field: 'shop_totle', title: __('Shop_totle')},
                        // {field: 'shop_description',width:'5%', title: __('Shop_description')},
                        {field: 'shop_skin', title: __('Shop_skin'), searchList: {"0":__('Shop_skin 0')}, formatter: Table.api.formatter.normal},
                        {field: 'shop_credit', title: __('Shop_credit')},
                        // {field: 'shop_site',width:'5%', title: __('Shop_site')},
                        {field: 'shop_type',title: __('Shop_type'), searchList: {"1":__('Shop_type 1'),"2":__('Shop_type 2'),"3":__('Shop_type 3'),"4":__('Shop_type 4'),"5":__('Shop_type 5')}, formatter: Table.api.formatter.normal},
                        {field: 'shop_coord', title: __('Shop_coord')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});