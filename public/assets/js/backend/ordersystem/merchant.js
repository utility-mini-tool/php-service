define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ordersystem/merchant/index',
                    add_url: 'ordersystem/merchant/add',
                    edit_url: 'ordersystem/merchant/edit',
                    del_url: 'ordersystem/merchant/del',
                    multi_url: 'ordersystem/merchant/multi',
                    table: 'merchant',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'created_from', title: __('Created_from')},
                        {field: 'created_at', title: __('Created_at'), operate:'RANGE', addclass:'datetimerange'},
                        // {field: 'deleted_at', title: __('Deleted_at'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'updated_at', title: __('Updated_at'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'updated_from', title: __('Updated_from')},
                        {field: 'merchant_name', title: __('Merchant_name')},
                        {field: 'merchant_form', title: __('Merchant_form')},
                        {field: 'merchant_phone', title: __('Merchant_phone')},
                        {field: 'merchant_site', title: __('Merchant_site')},
                        {field: 'merchant_class', title: __('Merchant_class'),
                        formatter: function (value, row, index){ // 单元格格式化函数
                              // return value;
                              return MerchantClass[value]['name'];
                          }
                        },
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});