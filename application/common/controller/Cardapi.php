<?php

namespace app\common\controller;

use think\Request;
use think\Controller;

/**
 * 打卡小程序 基类
 */
class Cardapi extends Controller
{

    // 允许放行的用户id
    private $allowtThePerson = ['防止key为0','oY9oA0Xkp6kvCETBQYJ3FBkcvqgc', 'test1','openid'];
    
    /**
     * 初始化函数
     *
     * identity验证
     */
    public function _initialize()
    {
      
        $input = input('get.');
        $request = request();
        // 必须存在一个
        if(isset($input['openid']) || isset($input['code'])){

            if(isset($input['openid']) && !array_search($input['openid'],$this->allowtThePerson)){

                return json(['code'=>0, 'msg'=>'你是谁1？','data'=>''])->send();
            }
            
            if(isset($input['code']) && $request->baseUrl() !=='/api/card/login'){

                return json(['code'=>0, 'msg'=>'你是谁2？','data'=>''])->send();
            }

        }else{
            // $this->errorReturn('你是谁3？');
            return json(['code'=>0, 'msg'=>'你是谁3？','data'=>''])->send();
        }
    }

    /**
     * 错误信息返回
     *
     * @access protected
     * @param  string        $msg     返回信息
     * @param  array        $data     返回数据（沉余备用）
     * @return array
     */
    protected function errorReturn($msg, $data = null)
    {
        return  json(['code'=>0, 'msg'=> $msg]);
    }

    /**
     * 成功信息返回
     *
     * @access protected
     * @param  string       $msg      返回信息
     * @param  array        $data     返回数据
     * @return array
     */
    protected function succeedReturn($msg, $data = null)
    {
        return  json(['code'=>200, 'msg'=> $msg, 'data'=>$data]);
    }

}
