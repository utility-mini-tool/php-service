<?php

namespace app\api\controller;

use think\Db;
use app\common\library\wechat\MiniProgram;
use app\common\controller\Cardapi;

class Card extends Cardapi
{
    


    public function _initialize()
    {
        // parent::_initialize();
    }

    /*
    * 登陆接口
    *
    */
    public function login()
    {

        $val = input('get.');
        $userInfo = MiniProgram::wechatDecode($val['code'],$val['encryptedData'],$val['iv']);
        // 存入登陆信息
        $request = request();
        $insert = array();
        $insert['method'] = $request->method();
        $insert['type'] = $request->type();
        $insert['ip'] = $request->ip();
        $insert['isAjax'] = $request->isAjax()? 1: 0;
        $insert['openId'] = $userInfo['openId'];
        $insert['logintime'] = date('Y-m-d H:i:s',time());
        Db::table('jigong_loginrecord')->insert($insert);
        return $this->succeedReturn('登陆成功！', $userInfo);
    }

    /*
    * 记录日志接口
    *
    */
    public function logslist()
    {
        $data = Db::table('jigong_record')->order('warn desc,time desc')->select();
        return  $this->succeedReturn('查询成功！', $data);
    }

    /*
    * 删除日志接口
    *
    */
    public function dellog()
    {
        $input  = input('post.');

        if(!$input['id']){

            return $this->errorReturn('删除失败，刷新一下再试试！');
        }

        $input['warn'] = 0;
        $input['delete_at'] = date('Y-m-d H:i:s',time());

        $data = Db::table('jigong_record')->update($input);

        if($data){

            return $this->succeedReturn('操作完成！', $data);
        }

        return $this->errorReturn('操作失败！');
    }

    /*
    * 修改日志状态接口
    *
    */
    public function updatelogstatus()
    {
        $input  = input('post.');

        if(!$input['id'] || !$input['warn']){

            return $this->errorReturn('修改失败，刷新一下再试试！');
        }

        $input['warn'] = $input['warn']==1? 3: 0;
        $data = Db::table('jigong_record')->update($input);

        if($data){

            return $this->succeedReturn('操作完成！');
        }

        return $this->errorReturn('操作失败！');
    }

    /*
    * 添加日志状态接口
    *
    */
    public function insertlog()
    {
        $input  = input('post.');

        if(!$input['record']){

            return $this->errorReturn('内容不可为空！');
        }

        $input['time'] = date('Y-m-d H:i:s',time());
        $input['warn'] = $input['warn']? 3: 0;

        $data = Db::table('jigong_record')->insert($input);

        if($data){

            return $this->succeedReturn('操作完成！');
        }

        return $this->errorReturn('操作失败！');
    }

    /*
    * 打卡记工首页接口
    *
    */
    public function cardindex()
    {

        // 获取最新的项目
        $projectid = Db::table('jigong_project')->where('uid',input('get.openid'))->order('id desc')->find();
        // 项目下工作人员
        $user = Db::table('jigong_user')->where('pid',$projectid['id'])->field('id,name')->select();
        // 项目下的打卡记录
        $projectCardLogs = Db::table('jigong_card')->where('pid',$projectid['id'])->field('info,time,userid,id,status')->select();
        // 获取首次打卡时间
        $projectCardMinTime = array_column($projectCardLogs,'time');
        $beginTime = strtotime(min($projectCardMinTime))/86400;
        $today = strtotime('today')/86400;
        $calendar = [];
        foreach ($user as $key => $item) {
            $calendar[$key]['user'] = $item;
            $workload = 0;
                // 下标
                $keyMun = 1;

            for ($i = $beginTime; $i <= $today; $i++) {
                $itemTime = date('Y-m-d',$i*86400);
                foreach ($projectCardLogs as $k => $v) {
                    if(isset($v['userid']) && $item['id'] == $v['userid'] && $v['time'] == $itemTime){
                        $calendar[$key]['user']['card'][$keyMun] = $v;
                        // 上班天数
                        $workload += $v['status'];
                    }
                }
                $calendar[$key]['user']['card'][$keyMun]['keytime'] = date('y/m/d',$i*86400);
                $keyMun++;

            }
            // 获取日期长度，用来控制表格宽度
            $calendar[$key]['user']['length'] = count($calendar[$key]['user']['card']);
            // 计算工作天数
            $calendar[$key]['user']['workload'] = $workload? $workload/10: 0;
        }
        
        // 首页提示提醒
        $cardAlert = Db::table('jigong_record')->order('warn desc,time desc')->find();
        $returnData = ['calendar' => $calendar, 'cardAlert' => $cardAlert, 'project'=>$projectid];
        return $this->succeedReturn('加载成功！', $returnData);
    }

    /*
    * 打卡操作接口
    *
    */
    public function cardoperate()
    {

        $input = input('post.');
        // 工作时间和人员不可以为空
        if(!isset($input['status']) && !$input['userid']){

            return $this->succeedReturn('请刷新重试！');
        }

        $timestamp = time();
        // 今日打卡
        if($input['card_type']){

            // 时间设为今天
            $input['tabtime'] = date('Y-m-d H:i:s',$timestamp);
            // 打卡时间
            $input['year']  = date('Y',$timestamp);
            $input['month'] = date('m',$timestamp);
            $input['time'] = date('Y-m-d',$timestamp);
            // 是否为修改
            $where = ['pid'=>$input['pid'], 'userid'=>$input['userid'], 'time'=>$input['time']];
            $issetCard = Db::table('jigong_card')->where($where)->value('id');
            $input['id'] = $issetCard;
        // 日历打卡
        }else{

            // 时间设为选择时间
            $strtotime = strtotime(str_replace("/","-",$input['time']));
            // 打卡时间
            $input['tabtime'] = date('Y-m-d H:i:s',$timestamp);
            $input['year']  = date('Y',$strtotime);
            $input['month'] = date('m',$strtotime);
            $input['time'] = date('Y-m-d',$strtotime);
        }

        // 删除沉余信息
        unset($input['card_type']);
        
        // 如果有id就是修改
        if($input['id']){

            $data = Db::table('jigong_card')->update($input);
            $msg = '修改成功！';
        }else{

            $data = Db::table('jigong_card')->insert($input);
            $msg = '修改失败！';
        }

        if($data){

            return $this->succeedReturn($msg);
        }

        return $this->errorReturn('操作失败！');
    }

    /*
    * 添加用户操作接口
    *
    */
    public function adduser()
    {
        $input = input('post.');

        // 名字必填
        if(!$input['name']){

            return $this->succeedReturn('请填写名字！');
        }

        // 加入人员
        $insert['name'] = $input['name'];
        $insert['time'] = date('Y-m-d H:i:s',time());
        $insert['pid'] = $input['pid']? $input['pid']: 1;

        $data = Db::table('jigong_user')->insert($insert);

        if($data){

            return $this->succeedReturn('操作完成');
        }

        return $this->errorReturn('操作失败！');
    }

     /*
    * 搜索记录操作接口
    *
    */
    public function searchLogs()
    {
        $keyword = input('post.keyword');

        // 名字必填
        if(!$keyword){

            return $this->errorReturn('请填写关键词！');
        }

        // 加入人员
        $data = Db::table('jigong_record')->where('record','like',"%".$keyword."%")->select();

        if($data){
            return $this->succeedReturn('查询成功！', $data);
        }

        return $this->errorReturn('查询失败！');
    }

    /*
    * 添加项目操作接口
    *
    */
    public function addproject()
    {
        $input = input('post.');
        $input['uid'] = input('get.openid');
        $input['create_at'] = date('Y-m-d H:i:s',time());
        // 名字必填
        if(!$input['name']){

            return $this->succeedReturn('请填写信息！');
        }
        // 删除无用信息
        unset($input['pid']);
        // 加入人员
        $data = Db::table('jigong_project')->insert($input);

        if($data){
            return $this->succeedReturn('操作完成！');
        }
        return $this->errorReturn('操作失败！');
    }

    
    /*
    * 项目首页接口
    *
    */
    public function project()
    {

        // 获取最新的项目
        $projectids = Db::table('jigong_project')->where('uid',input('get.openid'))->field('id,name,create_at')->order('id desc')->select();
        foreach ($projectids as $key => &$value) {
            // 项目下工作人员
            $user = Db::table('jigong_user')->where('pid',$value['id'])->field('id,name')->select();
            // 项目下的打卡记录
            $projectCardLogs = Db::table('jigong_card')->where('pid',$value['id'])->field('info,time,userid,id,status')->select();
            // 获取首次打卡时间
            $projectCardMinTime = array_column($projectCardLogs,'time');
            $beginTime = strtotime(min($projectCardMinTime))/86400;
            $today = strtotime('today')/86400;
            $calendar = array();
            foreach ($user as $key => $item) {
                $calendar[$key]['user'] = $item;
                // 上班天数
                $workload = 0;
                // 下标
                $keyMun = 1;

                for ($i = $beginTime; $i <= $today; $i++) {
                    $itemTime = date('Y-m-d',$i*86400);
                    foreach ($projectCardLogs as $k => $v) {
                        if(isset($v['userid']) && $item['id'] == $v['userid'] && $v['time'] == $itemTime){
                            $calendar[$key]['user']['card'][$keyMun] = $v;
                            // 上班天数
                            $workload += $v['status'];
                        }
                    }
                    $calendar[$key]['user']['card'][$keyMun]['keytime'] = date('y/m/d',$i*86400);
                    $keyMun++;

                }
                // 获取日期长度，用来控制表格宽度
                $value['length'] = count($calendar[$key]['user']['card']);
                // 计算工作天数
                $calendar[$key]['user']['workload'] = $workload? $workload/10: 0;
            }
            
            // 首页提示提醒
            $value['card'] = $calendar;
        }

        
        
     
        return $this->succeedReturn('加载成功！', $projectids);
    }

    /*
    * 项目首页接口
    *
    */
    public function loginRecord()
    {

        $loginList = Db::table('jigong_loginrecord')->where('openId',input('get.openid'))->limit(100)->column('logintime');   
        return $this->succeedReturn('加载成功！', $loginList);
    }

}
