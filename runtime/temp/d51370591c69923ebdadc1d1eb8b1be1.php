<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:79:"D:\XAMPP\htdocs\lms\public/../application/admin\view\ordersystem\shop\edit.html";i:1540472882;s:62:"D:\XAMPP\htdocs\lms\application\admin\view\layout\default.html";i:1536636085;s:59:"D:\XAMPP\htdocs\lms\application\admin\view\common\meta.html";i:1536636085;s:61:"D:\XAMPP\htdocs\lms\application\admin\view\common\script.html";i:1536636085;}*/ ?>
<!DOCTYPE html>
<html lang="<?php echo $config['language']; ?>">
    <head>
        <meta charset="utf-8">
<title><?php echo (isset($title) && ($title !== '')?$title:''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">

<link rel="shortcut icon" href="/assets/img/favicon.ico" />
<!-- Loading Bootstrap -->
<link href="/assets/css/backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.css?v=<?php echo \think\Config::get('site.version'); ?>" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
    var require = {
        config:  <?php echo json_encode($config); ?>
    };
</script>
    </head>

    <body class="inside-header inside-aside <?php echo defined('IS_DIALOG') && IS_DIALOG ? 'is-dialog' : ''; ?>">
        <div id="main" role="main">
            <div class="tab-content tab-addtabs">
                <div id="content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <section class="content-header hide">
                                <h1>
                                    <?php echo __('Dashboard'); ?>
                                    <small><?php echo __('Control panel'); ?></small>
                                </h1>
                            </section>
                            <?php if(!IS_DIALOG && !$config['fastadmin']['multiplenav']): ?>
                            <!-- RIBBON -->
                            <div id="ribbon">
                                <ol class="breadcrumb pull-left">
                                    <li><a href="dashboard" class="addtabsit"><i class="fa fa-dashboard"></i> <?php echo __('Dashboard'); ?></a></li>
                                </ol>
                                <ol class="breadcrumb pull-right">
                                    <?php foreach($breadcrumb as $vo): ?>
                                    <li><a href="javascript:;" data-url="<?php echo $vo['url']; ?>"><?php echo $vo['title']; ?></a></li>
                                    <?php endforeach; ?>
                                </ol>
                            </div>
                            <!-- END RIBBON -->
                            <?php endif; ?>
                            <div class="content">
                                <form id="edit-form" class="form-horizontal" role="form" data-toggle="validator" method="POST" action="">

    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Shop_status'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            
            <div class="radio">
            <?php if(is_array($shopStatusList) || $shopStatusList instanceof \think\Collection || $shopStatusList instanceof \think\Paginator): if( count($shopStatusList)==0 ) : echo "" ;else: foreach($shopStatusList as $key=>$vo): ?>
            <label for="row[shop_status]-<?php echo $key; ?>"><input id="row[shop_status]-<?php echo $key; ?>" name="row[shop_status]" type="radio" value="<?php echo $key; ?>" <?php if(in_array(($key), is_array($row['shop_status'])?$row['shop_status']:explode(',',$row['shop_status']))): ?>checked<?php endif; ?> /> <?php echo $vo; ?></label> 
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>

        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Shop_totle'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-shop_totle" data-rule="required" class="form-control form-control" name="row[shop_totle]" type="text" value="<?php echo $row['shop_totle']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Shop_description'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <textarea class="form-control" id="c-shop_description" name="row[shop_description]"><?php echo $row['shop_description']; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Shop_skin'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
                        
            <select  id="c-shop_skin" class="form-control selectpicker" name="row[shop_skin]">
                <?php if(is_array($shopSkinList) || $shopSkinList instanceof \think\Collection || $shopSkinList instanceof \think\Paginator): if( count($shopSkinList)==0 ) : echo "" ;else: foreach($shopSkinList as $key=>$vo): ?>
                    <option value="<?php echo $key; ?>" <?php if(in_array(($key), is_array($row['shop_skin'])?$row['shop_skin']:explode(',',$row['shop_skin']))): ?>selected<?php endif; ?>><?php echo $vo; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Shop_credit'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-shop_credit" class="form-control form-control" name="row[shop_credit]" type="number" value="<?php echo $row['shop_credit']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Shop_site'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <textarea class="form-control" id="c-shop_site" name="row[shop_description]"><?php echo $row['shop_site']; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Shop_type'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
                        
            <select  id="c-shop_type" class="form-control selectpicker" name="row[shop_type]">
                <?php if(is_array($shopTypeList) || $shopTypeList instanceof \think\Collection || $shopTypeList instanceof \think\Paginator): if( count($shopTypeList)==0 ) : echo "" ;else: foreach($shopTypeList as $key=>$vo): ?>
                    <option value="<?php echo $key; ?>" <?php if(in_array(($key), is_array($row['shop_type'])?$row['shop_type']:explode(',',$row['shop_type']))): ?>selected<?php endif; ?>><?php echo $vo; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Shop_coord'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-shop_coord" class="form-control form-control" name="row[shop_coord]" type="text" value="<?php echo $row['shop_coord']; ?>">
        </div>
    </div>
    <div class="form-group layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled"><?php echo __('OK'); ?></button>
            <button type="reset" class="btn btn-default btn-embossed"><?php echo __('Reset'); ?></button>
        </div>
    </div>
</form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/assets/js/require<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js" data-main="/assets/js/require-backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js?v=<?php echo $site['version']; ?>"></script>
    </body>
</html>