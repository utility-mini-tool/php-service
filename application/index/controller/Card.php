<?php

namespace app\index\controller;

use think\Db;
use app\common\controller\Frontend;
use app\common\library\Token;

class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
        // $this->redirect('jigong');
        return $this->view->fetch();
    }

    public function news()
    {
        $newslist = [];
        return jsonp(['newslist' => $newslist, 'new' => count($newslist), 'url' => 'https://www.fastadmin.net?ref=news']);
    }


    // 打卡页面
    public function jigong()
    {
        $data = Db::table('jigong_user')->field('id,name')->select();
        foreach ($data as $key => &$value) {
            for ($i=1; $i < 32; $i++) { 
                // 格式化时间
                $i = strlen($i) == 1? '0'.$i:$i;
                // 时间条件
                $cardwhere = ['time'=>date('Y-m-',time()).$i];
                // 整个月日期
                $index = date('Y-m-',time()).$i;
                $value['card'][$index] = Db::table('jigong_card')->where($cardwhere)->where(['userid'=>$value['id']])->find();
                $value['card'][$index]['nowtime'] = $index;
            }
            
        }

        return $this->view->fetch('',[
            'data'=> $data,
            'time'=>date('Y年m月',time()), 
            'title'=>Db::table('jigong_record')->order('warn desc,id desc')->find(),
            'year'=>Db::table('jigong_card')->group('year')->field('year')->select(),
            'month'=> Db::table('jigong_card')->group('month')->field('month')->select(),
        ]);
    }

    // 添加人员
    public function add()
    {
        // 名字必填
        if(!input('post.name'))   
            return ['code'=>0, 'msg'=>'请填写名字！'];
        // 加入人员
        $data = Db::table('jigong_user')->insert([ 'name'=>input('post.name'), 'time'=>date('Y-m-d H:i:s',time()) ]);

        if($data)
            return ['code'=>1, 'msg'=>'操作完成'];

        return ['code'=>0, 'msg'=>'操作失败！'];
    }

    // 打卡操作
    public function userdaka()
    {
        $input = input('post.');
        // 时间不可以为空
        if(!isset($input['status']))   
            return ['code'=>0, 'msg'=>'请选择记工时间'];
        // 人员不可以为空
        if(!$input['userid'])   
            return ['code'=>0, 'msg'=>'请刷新重试'];
        // 记录打卡时间
        $input['tabtime'] = date('Y-m-d H:i:s',time());
        $input['year']  = substr($input['time'],0,4);
        $input['month'] = substr($input['time'],5,2);
       // 如果有id就是修改
        if($input['id']){
            $data = Db::table('jigong_card')->update($input);
        }else{
            $data = Db::table('jigong_card')->insert($input);
        }
        if($data)
            return ['code'=>1, 'msg'=>'操作完成'];
        return ['code'=>0, 'msg'=>'操作失败！'];
    }

    // 添加 随笔笔记
    public function addrecord()
    {
        // 名字必填
        if(!input('post.record'))   
            return ['code'=>0, 'msg'=>'请填写要记录的文字！'];
        
        $data = Db::table('jigong_record')->insert([ 'record'=>input('post.record'), 'time'=>date('Y-m-d H:i:s',time()) ]);

        if($data)
            return ['code'=>1, 'msg'=>'操作完成'];

        return ['code'=>0, 'msg'=>'操作失败！'];
    }

    public function recordlog(){
        $data = Db::table('jigong_record')->order('warn desc,id desc')->paginate(20,true);
        return $this->view->fetch('',['data'=>$data]);
    }

    public function delrecordlog(){
        $input  = input('post.');
        if(!$input['id'])
            return ['code'=>0, 'msg'=>'删除失败，刷新一下再试试'];
        $input['warn'] = 0;
        $input['delete_at'] = date('Y-m-d H:i:s',time());
        $data = Db::table('jigong_record')->update($input);
        if($data)
            return ['code'=>1, 'msg'=>'操作完成'];
        return ['code'=>0, 'msg'=>'操作失败！'];
    }
}
