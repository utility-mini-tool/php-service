<?php

return [
    'Merchant_id'  =>  '商户id',
    'Shop_status'  =>  '店铺状态',
    'Shop_status 1'  =>  '开业',
    'Shop_status 2'  =>  '停业',
    'Shop_totle'  =>  '店铺名称',
    'Shop_description'  =>  '店铺描述',
    'Shop_skin'  =>  '店面皮肤',
    'Shop_skin 0'  =>  '默认皮肤',
    'Shop_credit'  =>  '店面信用',
    'Shop_site'  =>  '店面地址',
    'Shop_type'  =>  '店铺类型',
    'Shop_type 1'  =>  '快餐',
    'Shop_type 2'  =>  '西餐',
    'Shop_type 3'  =>  '火锅',
    'Shop_type 4'  =>  '川菜',
    'Shop_type 5'  =>  '粤菜',
    'Shop_coord'  =>  '店面坐标(x,y)'
];
