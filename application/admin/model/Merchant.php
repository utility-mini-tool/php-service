<?php

namespace app\admin\model;

use think\Model;
use think\Session;

class Merchant extends Model
{
    // 表名
    protected $name = 'merchant';
    
    // 定义时间戳字段名
    protected $createTime = 'created_at';
    protected $updateTime = 'updated_at';
    protected $insert = ['created_from'];  
    protected $update = ['updated_from'];  
    
    // 追加属性
    protected $append = [

    ];

    protected function setCreatedFromAttr()
    {
        return Session::get('admin.id');
    }

    protected function setUpdatedFromAttr()
    {
        return Session::get('admin.id');
    }
    

    







}
