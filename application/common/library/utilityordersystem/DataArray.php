<?php
namespace app\common\library\utilityordersystem;

/**
 * 点餐项目公用数据类
 */
class DataArray
{

    /**
     * 商家级别
     * merchant_class
     * @param int $ClassId 分类id
     * @return Array
     */
    public static function getMerchantClass($ClassId = null)
    {
        $MerchantClass = [
            1 => ['id' => 1, 'name' => '一级会员'],
            2 => ['id' => 2, 'name' => '二级会员'],
            3 => ['id' => 3, 'name' => '三级会员'],
            4 => ['id' => 4, 'name' => '四级会员']
        ];
        // 如果获取单条
        if ($ClassId){
            if($ClassId-1 > count($MerchantClass))
                return '';
            return $MerchantClass[$ClassId-1];
        }
        // 获取所有
        return $MerchantClass;
    }


    /**
     * 构造函数
     * @param array $options 参数
     * @access public
     */
    public function __construct($options = [])
    {
        if (!empty($options)) {
            $this->options = array_merge($this->options, $options);
        }
        if ($this->options['connection']) {
            $this->handler = \think\Db::connect($this->options['connection'])->name($this->options['table']);
        } else {
            $this->handler = \think\Db::name($this->options['table']);
        }
    }

    /**
     * 存储Token
     * @param   string $token Token
     * @param   int $user_id 会员ID
     * @param   int $expire 过期时长,0表示无限,单位秒
     * @return bool
     */
    public function set($token, $user_id, $expire = null)
    {
        $expiretime = !is_null($expire) && $expire !== 0 ? time() + $expire : 0;
        $token = $this->getEncryptedToken($token);
        $this->handler->insert(['token' => $token, 'user_id' => $user_id, 'createtime' => time(), 'expiretime' => $expiretime]);
        return TRUE;
    }



}
