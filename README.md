基于ThinkPHP5+Bootstrap的极速后台开发框架。
===============


## **Car打卡日志程序升级记录**

* 2018/11/10
    * 提交代码
* 2018/11/12
    * 添加card打卡记录应用
* 2018/11/15
    * 删除config配置文件
* 2018/11/16
    * 添加微信小程序模块获取openi接口
    * 添加微信小程序加密数据解密算法
    * 新建card小程序模块父类控制器，提高代码可读性&维护性
    * 添加身份识别模块，关闭开放模式
* 2018/11/18
    * 增加个人中心信息显示
	* 增加个人登录日志
	* 增加过往项目列表和详情
	* 修改打卡记录由按月显示改为按项目开始到当前时间
	* 修改首页样式
	* 修改首页交互逻辑
	* 增加访客信息记录

## **在线演示**

https://www.jimuworld.cn/admin

用户名：admin

密　码：123123

## **界面截图**
![控制台](http://lyh.jimuworld.cn/images/img.jpg  "控制台")

 
## **特别鸣谢**

感谢以下的项目,排名不分先后

ThinkPHP：http://www.thinkphp.cn

微信开放平台：https://mp.weixin.qq.com/

AdminLTE：https://adminlte.io

Bootstrap：http://getbootstrap.com

jQuery：http://jquery.com

Bootstrap-table：https://github.com/wenzhixin/bootstrap-table

Nice-validator: https://validator.niceue.com

SelectPage: https://github.com/TerryZ/SelectPage

FastAdmin: https://github.com/karson/FastAdmin

## 版权信息

遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。
