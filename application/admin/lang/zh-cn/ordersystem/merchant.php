<?php

return [
    'Id'  =>  '商户id',
    'Created_from'  =>  '创建人',
    'Created_at'  =>  '创建时间',
    'Deleted_at'  =>  '删除时间',
    'Updated_at'  =>  '更新时间',
    'Updated_from'  =>  '更新人',
    'Merchant_name'  =>  '商户名称',
    'Merchant_form'  =>  '商户联系人',
    'Merchant_phone'  =>  '联系电话',
    'Merchant_site'  =>  '商家地址',
    'Merchant_class'  =>  '商家级别'
];
